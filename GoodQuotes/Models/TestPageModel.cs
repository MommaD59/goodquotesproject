﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GoodQuotes.Models
{
    public class TestPageModel
    {
        public IList<CustomersModel> customers { get; set; }

        public Quotes quote { get; set; }
    }
}
