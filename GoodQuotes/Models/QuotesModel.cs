﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;

namespace GoodQuotes.Models
{
    public class Quotes
    {
        public int Id { get; set; }
        public string Quote { get; set; }
        public string Author { get; set; }
        public string Comments { get; set; }
       // public int UsedQuote { get; set; }
       
    }
}
