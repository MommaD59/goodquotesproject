﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace GoodQuotes.Models
{
    public class CustomersModel
    {
        //This is the UI Model for customer to fill out info
        [Display(Name ="First Name")]
        [Required(ErrorMessage = "You need to give your first name.")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        [Required(ErrorMessage = "You need to give your last name.")]
        public string LastName { get; set; }

        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email Address*")]
        [Required(ErrorMessage = "You need to give a valid email address.")]
        public string email { get; set; }

        [Display(Name = "Confirm Email Address")]
        [Compare("email", ErrorMessage = "The Email and Confirm Email must match")]
        public string Confirm_email { get; set; }
        public int Id { get; internal set; }

        
    }
}
