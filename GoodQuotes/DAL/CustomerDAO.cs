﻿using System.Collections.Generic;
using System.Linq;
using GoodQuotes.Models;
using System.Data.SqlClient;
using Dapper;
using System;

namespace GoodQuotes.DAL
{
    public class ICustomersSqlDAO : ICustomersDAO
    {
        private readonly string connectionString;
        public ICustomersSqlDAO(string connectionString)
        {
            this.connectionString = connectionString;
        }
      // Returns a list of all customers
        public IList<CustomersModel> GetAllCustomers()
        {
            //I am instantiating a new list of objects that holds data type cusomers from class customers.
            IList<CustomersModel> customers = new List<CustomersModel>();
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("SELECT * FROM Customers");
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        customers.Add(new CustomersModel()
                        {
                            Id = Convert.ToInt32(reader["Id"]),
                            FirstName = Convert.ToString(reader["FirstName"]),
                            LastName = Convert.ToString(reader["LastName"]),
                            email = Convert.ToString(reader["email"]),
                        });
                    }
                    
                }
            }
            catch (SqlException)
            {
                throw;
            }
            return customers;
        }

       
        //Saves a new customer to the database
          public int SaveCustomer(CustomersModel newCustomer)
          {
              try
              {
                  using (SqlConnection conn = new SqlConnection(connectionString))
                  {
                      conn.Open();
                      SqlCommand cmd = new SqlCommand("INSERT INTO Customers VALUES (@FirstName, @LastName, @email);", conn);
                      cmd.Parameters.AddWithValue("@FirstName", newCustomer.FirstName);
                      cmd.Parameters.AddWithValue("@LastName", newCustomer.LastName);
                      cmd.Parameters.AddWithValue("@email", newCustomer.email);

                      return cmd.ExecuteNonQuery();

                     

                  }
              }
              catch (SqlException ex)
              {
                  throw;
              }
          }
    }
}

