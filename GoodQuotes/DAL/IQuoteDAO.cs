﻿using GoodQuotes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;



namespace GoodQuotes.DAL
{
    public interface IQuotesDAO
    {
      Quotes GetNextUnusedQuote();
       
    }
}
