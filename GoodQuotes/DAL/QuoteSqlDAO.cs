﻿using GoodQuotes.Models;
using System;
using System.Data.SqlClient;
using Dapper;

namespace GoodQuotes.DAL
{
    public class QuotesSqlDAO : IQuotesDAO
    {
        private readonly string connectionString;

        public QuotesSqlDAO(string connectionString)
        {
            this.connectionString = connectionString;
        }

        private Quotes MapRowToQuotes(SqlDataReader reader)
        {
            return new Quotes()
            {
                Id = Convert.ToInt32(reader["Id"]),
                Quote = Convert.ToString(reader["Quote"]),
                Author = Convert.ToString(reader["Author"]),
                Comments = Convert.ToString(reader["Comments"])
                //UsedQuote = Convert.ToInt32(reader["UsedQuote"])
            };
        }     

        public Quotes GetNextUnusedQuote()             
        {
           //Instantiation
            Quotes quoteToday = new Quotes();             
            try
            {
                using (SqlConnection conn = new SqlConnection(connectionString))             
                {
                    conn.Open();
                    //@Id is for security reasons, so client cannot write straight to the database
                    SqlCommand cmd = new SqlCommand("SELECT TOP 1 * FROM Quotes WHERE UsedQuote = 0 ORDER BY Id ASC;", conn);
                    SqlDataReader reader = cmd.ExecuteReader();
                   /* quoteToday.Id = Convert.ToInt32(reader["Id"]); 
                    quoteToday.Quote = Convert.ToString(reader["Quote"]);
                    quoteToday.Author = Convert.ToString(reader["Author"]);
                    quoteToday.Comments = Convert.ToString(reader["Comments"]);*/
                    
                   /* SqlCommand cmd2 = new SqlCommand("UPDATE Quotes SET UsedQuote = 1 WHERE Id = @Id;", conn);

                    cmd2.Parameters.AddWithValue("@Id", quoteToday.Id);
                    cmd2.ExecuteNonQuery();*/
                  
                   
                }
            }
            catch (SqlException)
            {
                throw;
            }
            return quoteToday;
        }

    }
}


