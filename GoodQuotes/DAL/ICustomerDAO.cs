﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GoodQuotes.Models;

namespace GoodQuotes.DAL
{
    public interface ICustomersDAO

    {
      IList<CustomersModel> GetAllCustomers();

       // Saves a new customer to the System.
       int SaveCustomer(CustomersModel newCustomer);
    }
}
