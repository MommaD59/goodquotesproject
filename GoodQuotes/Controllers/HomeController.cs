﻿using GoodQuotes.DAL;
using GoodQuotes.Models;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;

namespace GoodQuotes.Controllers
{
    public class HomeController : Controller
    {
       /* 
        private ICustomerDAO CustomerDAO;

        //am I going to need this to get my customers to send the quotes too?
       public object customer { get; private set; }

        public HomeController(IQuotesDAO  QuotesDAO, ICustomerDAO CustomerDAO)
        {
            this.QuotesDAO = QuotesDAO;
            this.CustomerDAO = CustomerDAO;
        }*/

        
        public IActionResult Index()
        {
            return View();
        }

       /* [HttpGet]
        public IActionResult QuoteView()
        {
            
            return View();
        }
        public IActionResult Customer()
        {
            return View();
        }


        [HttpGet]
        public IActionResult GetNextUnusedQuote()
        {
            //TODO - FIX
            Quotes quoteToday = new Quotes();
            quoteToday = QuotesDAO.GetNextUnusedQuote();
            return View(quoteToday);
        }*/

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
