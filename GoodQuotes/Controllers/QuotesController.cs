﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using GoodQuotes.DAL;
using GoodQuotes.Models;

namespace GoodQuotes.Controllers
{
    public class QuotesController : Controller
    {
        private ICustomersDAO customersDAO;
        //private IQuotesDAO quotesDAO;

        public QuotesController(ICustomersSqlDAO customersDAO)
        {
            this.customersDAO = customersDAO;
            //this.quotesDAO = quotesDAO;
        }

        public IActionResult Index()
        {
           return View();
        }

        public IActionResult SignUp()
        {
            ViewBag.Message = "Customer Sign Up";
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SignUp(CustomersModel model)
        {

            if (ModelState.IsValid)
            {
                //I'm calling the SaveCustomer() Method from the DAO to insert(the set part of property) into the Customer table in db.
                customersDAO.SaveCustomer(model);
                return RedirectToAction("Confirmation", "Quotes");
            }
            return View();
        }

        //Redirect Page
        public IActionResult Confirmation()
        {
            ViewBag.Message = "Congratulations. You are now enrolled in the daily Good Quotes Program. You should start receiving your daily quote" +
                "within 24 to 48 hours";
            return View();
        }

       /* [HttpGet]
        public IActionResult TestPage()
        {
            ViewBag.Message = "Wohoo! The Customer List";

            var customers= customersDAO.GetAllCustomers();

            var testmodel = new TestPageModel();
            testmodel.customers = customers;
            //testmodel.quote = quotesDAO.GetNextUnusedQuote();
           
            return View( customers);
        }*/
    }
}